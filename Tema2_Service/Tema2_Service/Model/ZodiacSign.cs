﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema2_Service.Model
{
    public class ZodiacSign
    {

        public ZodiacSign(DateTime minDate,DateTime maxDate,string sign)
        {
            this.minDate = minDate;
            this.maxDate = maxDate;
            this.sign = sign;
        }

        private DateTime minDate;
        public DateTime MinDate
        {
            get
            {
                return minDate;
            }
            set
            {
                minDate = value;
            }
        }

        private DateTime maxDate;
        public DateTime MaxDate
        {
            get
            {
                return maxDate;
            }
            set
            {
                maxDate = value;
            }
        }

        private string sign;
        public string Sign
        {
            get
            {
                return sign;
            }
            set
            {
                sign = value;
            }
        }
    }
}
