﻿using Grpc.Core;
using Grpc.Net.Client;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Tema2_Service.Services
{
    public class GateService:Gate.GateBase
    {
        enum Seasons
        {
            SPRING,
            SUMMER,
            AUTUMN,
            WINTER
        };

        private Seasons getSeason(DateTime date)
        {
            if(date.Month>=3 && date.Month<6)
            {
                return Seasons.SPRING;
            }

            if (date.Month >= 6 && date.Month < 9)
            {
                return Seasons.SUMMER;
            }

            if (date.Month >= 9 && date.Month < 12)
            {
                return Seasons.AUTUMN;
            }
            return Seasons.WINTER;
        }

        public override Task<ZodiacSign> GetZodiacSign(Birthdate request, ServerCallContext context)
        {

            DateTime dateTime;

            DateTime.TryParseExact(request.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,out dateTime);
            DateTime.TryParseExact(request.Date, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,out dateTime);
            
            

            if(getSeason(dateTime)==Seasons.SPRING)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Spring.SpringClient(channel);

                var date = new SpringDate() { Date = request.Date };
                var response = client.GetSpringSign(date);

                return Task.FromResult(new ZodiacSign() { Sign = response.Sign });
            }

            if (getSeason(dateTime) == Seasons.SUMMER)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Summer.SummerClient(channel);

                var date = new SummerDate() { Date = request.Date };
                var response = client.GetSummerSign(date);

                return Task.FromResult(new ZodiacSign() { Sign = response.Sign });
            }

            if (getSeason(dateTime) == Seasons.AUTUMN)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Autumn.AutumnClient(channel);

                var date = new AutumnDate() { Date = request.Date };
                var response = client.GetAutumnSign(date);

                return Task.FromResult(new ZodiacSign() { Sign = response.Sign });
            }

            if (getSeason(dateTime) == Seasons.WINTER)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Winter.WinterClient(channel);

                var date = new WinterDate() { Date = request.Date };
                var response = client.GetWinterSign(date);

                return Task.FromResult(new ZodiacSign() { Sign = response.Sign });
            }



            return Task.FromResult(new ZodiacSign() { Sign = "NONE" });
        }
    }
}
