﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Tema2_Service.Services
{
    public class WinterService:Winter.WinterBase
    {
        public bool IsInsideInterval(DateTime date, Model.ZodiacSign zodiac)
        {

            if(zodiac.MaxDate.Month<zodiac.MinDate.Month)
            {
                DateTime newDate = new DateTime(2, date.Month, date.Day);
                DateTime newMaxDate = new DateTime(3, zodiac.MaxDate.Month, zodiac.MaxDate.Day);
                DateTime newMinDate = new DateTime(1, zodiac.MinDate.Month, zodiac.MinDate.Day);

                if (newDate >= newMinDate && newDate < newMaxDate)
                {
                    return true;
                }
            }
            else
            {
                DateTime newDate = new DateTime(1, date.Month, date.Day);
                DateTime newMaxDate = new DateTime(1, zodiac.MaxDate.Month, zodiac.MaxDate.Day);
                DateTime newMinDate = new DateTime(1, zodiac.MinDate.Month, zodiac.MinDate.Day);

                if (newDate >= newMinDate && newDate < newMaxDate)
                {
                    return true;
                }
            }
            
            
            return false;
        }

        public string GetSign(List<Model.ZodiacSign> signs, DateTime dateTime)
        {
            string sign = "";

            foreach (Model.ZodiacSign zodiacSign in signs)
            {
                if (IsInsideInterval(dateTime, zodiacSign))
                {
                    sign = zodiacSign.Sign;
                }
            }

            return sign;
        }

        public override Task<WinterSign> GetWinterSign(WinterDate request, ServerCallContext context)
        {
            List<Model.ZodiacSign> signs = new List<Model.ZodiacSign>();

            StreamReader stream = new StreamReader(@"Resources\winter.txt");

            string line;

            while ((line = stream.ReadLine()) != null)
            {
                string[] elements = line.Split(' ');

                signs.Add(new Model.ZodiacSign(DateTime.Parse(elements[0]), DateTime.Parse(elements[1]), elements[2]));
            }

            DateTime dateTime;
            DateTime.TryParseExact(request.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
            DateTime.TryParseExact(request.Date, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            string sign = GetSign(signs,dateTime);

            return Task.FromResult(new WinterSign() { Sign = sign });
        }
    }
}
