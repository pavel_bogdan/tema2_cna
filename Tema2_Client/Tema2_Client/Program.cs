﻿using Grpc.Net.Client;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Tema2_Client
{
    class Program
    {
        static bool dateValidation(string date)
        {

            string[] formats =
            {
                "MM/dd/yyyy",
                "M/d/yyyy"
            };

            foreach(string format in formats)
            {
                if(DateTime.TryParseExact(date,format,CultureInfo.InvariantCulture,DateTimeStyles.None,out _))
                {
                    return true;
                }
            }
            return false;
        }


        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Gate.GateClient(channel);

            Console.WriteLine("Enter date: ");
            string date = Console.ReadLine();


            if(dateValidation(date))
            {
                var birthdate = new Birthdate { Date = date };

                var response = await client.GetZodiacSignAsync(birthdate);

                Console.WriteLine("Zodiac sign: "+response.Sign);
            }
            else
            {
                Console.WriteLine("WRONG DATE !");
            }
           
        }
    }
}
